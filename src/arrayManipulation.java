import java.util.Arrays;
import java.util.Collections;
public class arrayManipulation {
    public static void main(String[] args) {
        int[]numbers = {5, 8, 3, 2, 7};
        String[]names = {"Alice", "Bob", "Charlie", "David"};
        double[]values = new double[4];
        
        System.out.println("Numbers");
        for(int i=0; i<numbers.length; i++){
            System.out.print(numbers[i] + " ");
        }

        System.out.println("");
        System.out.println("Names");
        for(int i=0; i<names.length; i++){
            System.out.print(names[i] + " ");
        }

        values[0] = 20.0;
        values[1] = 6.0;
        values[2] = 7.0;
        values[3] = 11.0;

        int sum = 0;
        for(int i=0; i<numbers.length; i++){
            sum = sum + numbers[i];
        }
        System.out.println("");
        System.out.println("sum = " + sum);

        double max = 0.0;
        for(int i=0; i<values.length; i++){
            if(max < values[i]){
                max = values[i];
            }
        }
        System.out.println("max = " + max); 

        String[]reversedNames = new String[4];
        for(int i=0; i<reversedNames.length; i++){
           reversedNames[i] = names[i]; 
        }
        Arrays.sort(reversedNames, Collections.reverseOrder());
        for(int i=0; i<reversedNames.length; i++){
            System.out.print(reversedNames[i] + " ");
        }

        Arrays.sort(numbers);
        System.out.println("");
        System.out.println("Ascending Numbers");
        for(int i=0; i<numbers.length; i++){
            System.out.print(numbers[i] + " ");
        }
    }
}
