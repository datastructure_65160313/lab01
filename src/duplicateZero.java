public class duplicateZero {
    public static void main(String[] args) {
        int[]arr = {1, 0, 2, 3, 0, 4, 5, 0};
        for(int i = arr.length - 1; i >= 0; i--){
            if(arr[i] == 0){
                for(int j = arr.length - 1; j > i; j--){
                    arr[j] = arr[j-1];
                }
            } 
        }
        for(int i = 0; i < arr.length; i++){
            System.out.println(arr[i]);
        }
    }
}
